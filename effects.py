import sox
import os


'''
This class will add effects to the wav samples
The effects suggesteed where 
volume, chorus, delay, flanger, phaser, overdrive, lowpass, reverb 

The user selects edit, then a letter key. This will isnatiate the class
The user will then select the the effects(with params) to be added by using the knobs.
'''

class Effects(object):
    tfm = sox.Transformer()
    location_of_default_track = 'unknown'
    location_of_effects_track = 'unknown'




    def __init__(self, default_loc, effects_loc):#todo take in the location the track to be modified
        '''
        the locations of the track to be changed and where the new track is to be written

        '''
        self.location_of_default_track = default_loc
        self.location_of_effects_track = effects_loc









    def effects_to_be_added(self, effects):

       # effects is a dictionary with effect name and value of params to be added

        for k, v in effects.items():
            if k is 'reverb':
                Effects.add_reverb(self,v)
            elif k is 'lowpass':
                Effects.add_lowpass(self,v)
            elif k is 'overdrive':
                Effects.add_overdrive(self,v)
            elif k is 'phaser':
                Effects.add_phaser(self,v)
            elif k is 'flanger':
                Effects.add_flanger(self,v)
            elif k is 'delay':
                v = v/100
                Effects.add_delay(self,v)
            elif k is 'chorus':
                v = v/1000
                Effects.add_chorus(self,v)
            elif k is 'volume':
                Effects.add_volume(self ,v)
            elif k is 'trim':
                Effects.add_trim(self, v)


    def add_reverb(self, param):
       self.tfm.reverb(reverberance=param)

    def add_lowpass(self, param):
       self.tfm.lowpass(param)

    def add_overdrive(self, param):
        self.tfm.overdrive(param)

    def add_phaser(self, param):
        self.tfm.phaser(param)

    def add_flanger(self, param):
        self.tfm.flanger(param)

    def add_delay(self, param):

        self.tfm.delay([param])

    def add_chorus(self, param):
        self.tfm.chorus(param)

    def add_volume(self, param):
        self.tfm.vol(param)

    def add_trim(self, param):
        self.tfm.trim(0, param)

    def get_effects(self):
        #returns a list of applied effects
        return self.tfm.effects_log







    def apply_effects_to_file(self):
        #applies effects to a file and writes it to required location
        #the intial track is not affected a new track with effects added is created at the required location
        try:
            os.remove(self.location_of_effects_track)
        except:
            print(self.location_of_effects_track,"not deleted")

        #
        # #list of locations for wav files with effects:
        #
        # b1effects = 'samples//Test//Processed//b1'
        # b2effects = 'samples//Test//Processed//b2'
        # b3effects = 'samples//Test//Processed//b3'
        # b4effects = 'samples//Test//Processed//b4'
        # b5effects = 'samples//Test//Processed//b5'
        # b6effects = 'samples//Test//Processed//b6'
        # b7effects = 'samples//Test//Processed//b7'
        # b8effects = 'samples//Test//Processed//b8'
        # b9effects = 'samples//Test//Processed//b9'
        # b10effects = 'samples//Test//Processed//b10'
        # b11effects = 'samples//Test//Processed//b11'
        # b12effects = 'samples//Test//Processed//b12'
        # b13effects = 'samples//Test//Processed//b13'
        # b14effects = 'samples//Test//Processed//b14'
        # b15effects = 'samples//Test//Processed//b15'
        # b16effects = 'samples//Test//Processed//b16'
        #
        # loction_of_effects_wav_for_each_button = [
        #     b1effects,b2effects,b3effects,b4effects,b5effects,b6effects,b7effects,b8effects,b9effects,b10effects,
        #     b11effects,b12effects,b13effects,b14effects,b15effects,b16effects
       # ]
       #  self.tfm.build(
       #     'samples//Test//Originals//onclassical_demo_demicheli_geminiani_pieces_allegro-in-f-major_small-version.wav',
       #       loction_of_effects_wav_for_each_button[self.button_num])
        self.tfm.build(
            self.location_of_default_track,
            self.location_of_effects_track)
        self.tfm.clear_effects()

        effectsList = self.get_effects()

        print("Effects applied")
        for s in effectsList:
            print(s,"\n")

























# #remove previous file
# try:
#  os.remove('samples//Test//Processed//with_reverb.wav')
# except:
#     pass
#
# # Initialize a transfromer with the intended file and where we wish the changed file to be created
# tfm = sox.Transformer()
#
# # add reverb with defaults
# tfm.reverb(100, 100, wet_gain=9)
# #tfm.chorus(n_voices= 2, shapes=[s,t])
#
# # add fade
#
# # tfm.fade(fade_in_len=1.0,
# # fade_out_len=0.5)
#
# # tfm.trim(0.00 , 15)
#
#
#
# # create output file
# tfm.build(
#     'samples//Test//Originals//onclassical_demo_demicheli_geminiani_pieces_allegro-in-f-major_small-version.wav',
#     'samples//Test//Processed//with_reverb.wav')
#




