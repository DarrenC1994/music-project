from PySide import QtCore, QtGui
import sampler_gui
import sys
from pad import pad
from effects import Effects
import os
import time
import threading
import looping
import mido


class sampler(QtGui.QMainWindow, sampler_gui.Ui_MainWindow):

    def __init__(self, parent=None):
        super(sampler, self).__init__(parent)
        self.setupUi(self)

        self.loop_flag = False
        self.loop = []
        
        self.button_flash()

        # the default track to be played on clicking a button
        pad_1_track = "kick.wav"
        pad_2_track = "snare.wav"
        pad_3_track = "hh.wav"
        pad_4_track = "samples/Drums/Metallic/Agogo Digital DDM220 1.wav"
        pad_5_track = "samples/Drums/Metallic/Cow TapeEcho V1.wav"
        pad_6_track = "samples/Drums/Metallic/Triangle DC2 3.wav"
        pad_7_track = "samples/Drums/Snare/Rim Analog DR55.wav"
        pad_8_track = "samples/Drums/Metallic/Cow GBTattoo.wav"
        pad_9_track = "samples/Drums/Snare/Snare Live 6 bit Meat.wav"
        pad_10_track = "samples/Drums/Snare/Snare Mpc60 clipped 016.wav"
        pad_11_track = "samples/Drums/Tom/Tom Analog JX3P 3.wav"
        pad_12_track = "samples/Drums/Tom/Tom RealBlueDrumA3.wav"
        pad_13_track = "samples/Drums/Wooden/Clave Analog MS20.wav"
        pad_14_track = "samples/Drums/Wooden/Perc Tass Dann 1.wav"
        pad_15_track = "samples/Drums/Shaker/Shaker Digital RX5 1.wav"
        pad_16_track = "samples/Drums/Shaker/Shaker Buki Kat.wav"

        # The file to add effects to
        pad_1_fx = 'samples//Test//Processed//b1.wav'
        pad_2_fx = 'samples//Test//Processed//b2.wav'
        pad_3_fx = 'samples//Test//Processed//b3.wav'
        pad_4_fx = 'samples//Test//Processed//b4.wav'
        pad_5_fx = 'samples//Test//Processed//b5.wav'
        pad_6_fx = 'samples//Test//Processed//b6.wav'
        pad_7_fx = 'samples//Test//Processed//b7.wav'
        pad_8_fx = 'samples//Test//Processed//b8.wav'
        pad_9_fx = 'samples//Test//Processed//b9.wav'
        pad_10_fx = 'samples//Test//Processed//b10.wav'
        pad_11_fx = 'samples//Test//Processed//b11.wav'
        pad_12_fx = 'samples//Test//Processed//b12.wav'
        pad_13_fx = 'samples//Test//Processed//b13.wav'
        pad_14_fx = 'samples//Test//Processed//b14.wav'
        pad_15_fx = 'samples//Test//Processed//b15.wav'
        pad_16_fx = 'samples//Test//Processed//b16.wav'

        self.pad_1 = pad(pad_1_track, pad_1_fx)
        self.pad_2 = pad(pad_2_track, pad_2_fx)
        self.pad_3 = pad(pad_3_track, pad_3_fx)
        self.pad_4 = pad(pad_4_track, pad_4_fx)
        self.pad_5 = pad(pad_5_track, pad_5_fx)
        self.pad_6 = pad(pad_6_track, pad_6_fx)
        self.pad_7 = pad(pad_7_track, pad_7_fx)
        self.pad_8 = pad(pad_8_track, pad_8_fx)
        self.pad_9 = pad(pad_9_track, pad_9_fx)
        self.pad_10 = pad(pad_10_track, pad_10_fx)
        self.pad_11 = pad(pad_11_track, pad_11_fx)
        self.pad_12 = pad(pad_12_track, pad_12_fx)
        self.pad_13 = pad(pad_13_track, pad_13_fx)
        self.pad_14 = pad(pad_14_track, pad_14_fx)
        self.pad_15 = pad(pad_15_track, pad_15_fx)
        self.pad_16 = pad(pad_16_track, pad_16_fx)

        self.midi_input = Worker()
        self.midi_input.start()
        self.midi_input.playSignal.connect(self.midi_play)

        self.pad_1_btn.clicked.connect(lambda: self.pad_clicked(self.pad_1))
        self.pad_2_btn.clicked.connect(lambda: self.pad_clicked(self.pad_2))
        self.pad_3_btn.clicked.connect(lambda: self.pad_clicked(self.pad_3))
        self.pad_4_btn.clicked.connect(lambda: self.pad_clicked(self.pad_4))
        self.pad_5_btn.clicked.connect(lambda: self.pad_clicked(self.pad_5))
        self.pad_6_btn.clicked.connect(lambda: self.pad_clicked(self.pad_6))
        self.pad_7_btn.clicked.connect(lambda: self.pad_clicked(self.pad_7))
        self.pad_8_btn.clicked.connect(lambda: self.pad_clicked(self.pad_8))
        self.pad_9_btn.clicked.connect(lambda: self.pad_clicked(self.pad_9))
        self.pad_10_btn.clicked.connect(lambda: self.pad_clicked(self.pad_10))
        self.pad_11_btn.clicked.connect(lambda: self.pad_clicked(self.pad_11))
        self.pad_12_btn.clicked.connect(lambda: self.pad_clicked(self.pad_12))
        self.pad_13_btn.clicked.connect(lambda: self.pad_clicked(self.pad_13))
        self.pad_14_btn.clicked.connect(lambda: self.pad_clicked(self.pad_14))
        self.pad_15_btn.clicked.connect(lambda: self.pad_clicked(self.pad_15))
        self.pad_16_btn.clicked.connect(lambda: self.pad_clicked(self.pad_16))
        self.play_button.toggled.connect(self.play_flash)
        self.edit_button.toggled.connect(self.edit_flash)
        self.load_button.toggled.connect(self.load_flash)

    def pad_clicked(self, pad):

        if self.play_button.isChecked():
            pad.play()
            self.check_loop(pad)

        elif self.edit_button.isChecked():

            pad.effect_flag = True
            reverb_value = self.Reverb.value()
            chorus_value = self.Chorus.value()
            delay_value = self.Delay.value()
            volume_value = self.Volume.value()

            effects_dict = {}
            if reverb_value > 0:
                effects_dict.update({"reverb": reverb_value})
            if chorus_value > 0:
                effects_dict.update({"chorus": chorus_value})
            if delay_value > 0:
                effects_dict.update({"delay": delay_value})
            if volume_value > 0:
                effects_dict.update({"volume": volume_value})
                print(effects_dict)

            if not effects_dict:
                print("Removing effects")
                pad.effect_flag = False
                try:
                    os.remove(pad.effect_fname)
                except:
                    print("file does not exist")
            else:
                self.add_effects(pad.wav_fname, pad.effect_fname, effects_dict)
                effects_dict.clear()

            pad.play()

        elif self.load_button.isChecked():
            pass

    def add_effects(self, default_loc, effects_loc, effects_dict):

        ef = Effects(default_loc, effects_loc)
        ef.effects_to_be_added(effects_dict)
        ef.apply_effects_to_file()

    def keyPressEvent(self, e):

        if e.text() in ("z"):
            self.pad_1_btn.animateClick()

        elif e.text() in ("x"):
            self.pad_2_btn.animateClick()

        elif e.text() in ("c"):
            self.pad_3_btn.animateClick()

        elif e.text() in ("v"):
            self.pad_4_btn.animateClick()

        elif e.text() in ("a"):
            self.pad_5_btn.animateClick()

        elif e.text() in ("s"):
            self.pad_6_btn.animateClick()

        elif e.text() in ("d"):
            self.pad_7_btn.animateClick()

        elif e.text() in ("f"):
            self.pad_8_btn.animateClick()

        elif e.text() in ("q"):
            self.pad_9_btn.animateClick()

        elif e.text() in ("w"):
            self.pad_10_btn.animateClick()

        elif e.text() in ("e"):
            self.pad_11_btn.animateClick()

        elif e.text() in ("r"):
            self.pad_12_btn.animateClick()

        elif e.text() in ("1"):
            self.pad_13_btn.animateClick()

        elif e.text() in ("2"):
            self.pad_14_btn.animateClick()

        elif e.text() in ("3"):
            self.pad_15_btn.animateClick()

        elif e.text() in ("4"):
            self.pad_16_btn.animateClick()

        elif e.text() in ("o"):
            self.edit_button.animateClick()

        elif e.text() in ("p"):
            self.play_button.animateClick()

        elif e.text() in ("i"):
            if self.loop_flag:
                self.loop_flag = False
            else:
                self.loop_flag = True
                self.loop_start = time.time()
                self.loop.append((0, 0))

        elif e.text() in ("k"):
            threading.Thread(target=looping.play_loop,
                             args=[self.loop]).start()

    def midi_play(self, msg):

            if hasattr(msg, 'control'):
                if msg.control is 112 and msg.value is 127:
                    self.pad_1_btn.animateClick()

                elif msg.control is 113  and msg.value is 127:
                    self.pad_2_btn.animateClick()

                elif msg.control is 114 and msg.value is 127:
                    self.pad_3_btn.animateClick()

                elif msg.control is 115 and msg.value is 127:
                    self.pad_4_btn.animateClick()

                elif msg.control is 116 and msg.value is 127:
                    self.pad_5_btn.animateClick()

                elif msg.control is 117 and msg.value is 127:
                    self.pad_6_btn.animateClick()

                elif msg.control is 118 and msg.value is 127:
                    self.pad_7_btn.animateClick()

                elif msg.control is 119 and msg.value is 127:
                    self.pad_8_btn.animateClick()

                elif msg.control is 120 and msg.value is 127:
                    self.pad_9_btn.animateClick()

                elif msg.control is 121 and msg.value is 127:
                    self.pad_10_btn.animateClick()

                elif msg.control is 122 and msg.value is 127:
                    self.pad_11_btn.animateClick()

                elif msg.control is 123 and msg.value is 127:
                    self.pad_12_btn.animateClick()

                elif msg.control is 124 and msg.value is 127:
                    self.pad_13_btn.animateClick()

                elif msg.control is 125 and msg.value is 127:
                    self.pad_14_btn.animateClick()

                elif msg.control is 126 and msg.value is 127:
                    self.pad_15_btn.animateClick()

                elif msg.control is 127 and msg.value is 127:
                    self.pad_16_btn.animateClick()
            #### for NANOPAD AND PROBABLY ALL MIDI CONTROLLERS ###

            elif hasattr(msg, 'note'):
                if msg.note is 36 and msg.velocity is 64:
                    self.pad_1_btn.animateClick()

                elif msg.note is 37 and msg.velocity is 64:
                    self.pad_2_btn.animateClick()

                elif msg.note is 38 and msg.velocity is 64:
                    self.pad_3_btn.animateClick()

                elif msg.note is 39 and msg.velocity is 64:
                    self.pad_4_btn.animateClick()

                elif msg.note is 40 and msg.velocity is 64:
                    self.pad_5_btn.animateClick()

                elif msg.note is 41 and msg.velocity is 64:
                    self.pad_6_btn.animateClick()

                elif msg.note is 42 and msg.velocity is 64:
                    self.pad_7_btn.animateClick()

                elif msg.note is 43 and msg.velocity is 64:
                    self.pad_8_btn.animateClick()

                elif msg.note is 44 and msg.velocity is 64:
                    self.pad_9_btn.animateClick()

                elif msg.note is 45 and msg.velocity is 64:
                    self.pad_10_btn.animateClick()

                elif msg.note is 46 and msg.velocity is 64:
                    self.pad_11_btn.animateClick()

                elif msg.note is 47 and msg.velocity is 64:
                    self.pad_12_btn.animateClick()

                elif msg.note is 48 and msg.velocity is 64:
                    self.pad_13_btn.animateClick()

                elif msg.note is 49 and msg.velocity is 64:
                    self.pad_14_btn.animateClick()

                elif msg.note is 50 and msg.velocity is 64:
                    self.pad_15_btn.animateClick()

                elif msg.note is 51 and msg.velocity is 64:
                    self.pad_16_btn.animateClick()


    def check_loop(self, pad):

        if self.loop_flag:
            time_diff = time.time() - self.loop_start
            self.loop.append((time_diff, pad))

    def button_flash(self):
                print('button flash')
                self.pad_16_btn.animateClick(msec=2000)

                self.pad_15_btn.animateClick(msec=1900)

                self.pad_14_btn.animateClick(msec=1800)

                self.pad_13_btn.animateClick(msec=1700)
                # time.sleep(.33)
                self.pad_9_btn.animateClick(msec=1600)
                # time.sleep(.33)
                self.pad_5_btn.animateClick(msec=1500)
                # time.sleep(.33)
                self.pad_1_btn.animateClick(msec=1400)
                # time.sleep(.33)
                self.pad_2_btn.animateClick(msec=1300)
                # time.sleep(.33)
                self.pad_3_btn.animateClick(msec=1200)
                # time.sleep(.33)
                self.pad_4_btn.animateClick(msec=1100)
                # time.sleep(.33)
                self.pad_8_btn.animateClick(msec=1000)
                # time.sleep(.33)
                self.pad_12_btn.animateClick(msec=900)
                # time.sleep(.33)
                self.pad_11_btn.animateClick(msec=800)
                # time.sleep(.33)
                self.pad_10_btn.animateClick(msec=700)
                # time.sleep(.33)
                # self.pad_7_btn.animateClick(msec=2000)
                # self.pad_6_btn.animateClick(msec=600)
                # self.pad_10_btn.animateClick(msec=700)
                # self.pad_11_btn.animateClick(msec=800)
                # self.pad_12_btn.animateClick(msec=900)
                # self.pad_8_btn.animateClick(msec=1000)
                # self.pad_4_btn.animateClick(msec=1100)
                # self.pad_3_btn.animateClick(msec=1200)
                # self.pad_2_btn.animateClick(msec=1300)
                # self.pad_1_btn.animateClick(msec=1400)
                # self.pad_5_btn.animateClick(msec=1500)
                # self.pad_9_btn.animateClick(msec=1600)
                # self.pad_13_btn.animateClick(msec=1700)
                # self.pad_14_btn.animateClick(msec=1800)
                # self.pad_15_btn.animateClick(msec=1900)
                # self.pad_16_btn.animateClick(msec=2000)

    def record_flash(self):
        print('edit flash called')
        self.pad_13_btn.animateClick(msec=1000)
        self.pad_9_btn.animateClick(msec=1000)
        self.pad_5_btn.animateClick(msec=1000)
        self.pad_1_btn.animateClick(msec=1000)
        self.pad_14_btn.animateClick(msec=1000)
        self.pad_15_btn.animateClick(msec=1000)
        self.pad_16_btn.animateClick(msec=1000)
        self.pad_12_btn.animateClick(msec=10)
        self.pad_11_btn.animateClick(msec=10)
        self.pad_10_btn.animateClick(msec=10)
        self.pad_2_btn.animateClick(msec=10)
        self.pad_3_btn.animateClick(msec=10)
        self.pad_4_btn.animateClick(msec=10)
        self.pad_6_btn.animateClick(msec=10)
        self.pad_7_btn.animateClick(msec=10)
        self.pad_8_btn.animateClick(msec=10)

    def load_flash(self):
        print('load flash called')
        self.pad_13_btn.animateClick(msec=1000)
        self.pad_14_btn.animateClick(msec=10)
        self.pad_9_btn.animateClick(msec=1000)
        self.pad_10_btn.animateClick(msec=10)
        self.pad_5_btn.animateClick(msec=1000)
        self.pad_6_btn.animateClick(msec=10)
        self.pad_1_btn.animateClick(msec=1000)
        self.pad_2_btn.animateClick(msec=1000)
        self.pad_7_btn.animateClick(msec=10)
        self.pad_8_btn.animateClick(msec=10)
        self.pad_3_btn.animateClick(msec=1000)
        self.pad_4_btn.animateClick(msec=1000)
        self.pad_15_btn.animateClick(msec=10)
        self.pad_16_btn.animateClick(msec=10)
        self.pad_11_btn.animateClick(msec=10)
        self.pad_12_btn.animateClick(msec=10)

    def edit_flash(self):
        print('edit flash called')
        self.pad_13_btn.animateClick(msec=1000)
        self.pad_9_btn.animateClick(msec=1000)
        self.pad_5_btn.animateClick(msec=1000)
        self.pad_1_btn.animateClick(msec=1000)
        self.pad_14_btn.animateClick(msec=1000)
        self.pad_15_btn.animateClick(msec=1000)
        self.pad_16_btn.animateClick(msec=1000)
        self.pad_12_btn.animateClick(msec=1000)
        self.pad_11_btn.animateClick(msec=1000)
        self.pad_10_btn.animateClick(msec=1000)
        self.pad_2_btn.animateClick(msec=1000)
        self.pad_3_btn.animateClick(msec=1000)
        self.pad_4_btn.animateClick(msec=1000)
        self.pad_6_btn.animateClick(msec=10)
        self.pad_7_btn.animateClick(msec=10)
        self.pad_8_btn.animateClick(msec=10)

    def play_flash(self):
        print('play flash called')
        self.pad_14_btn.animateClick(msec=1000)
        self.pad_10_btn.animateClick(msec=1000)
        self.pad_6_btn.animateClick(msec=10)
        self.pad_2_btn.animateClick(msec=1000)
        self.pad_11_btn.animateClick(msec=1000)
        self.pad_12_btn.animateClick(msec=10)
        self.pad_15_btn.animateClick(msec=1000)
        self.pad_16_btn.animateClick(msec=10)
        self.pad_8_btn.animateClick(msec=10)
        self.pad_4_btn.animateClick(msec=10)
        self.pad_7_btn.animateClick(msec=10)
        self.pad_3_btn.animateClick(msec=10)
        self.pad_2_btn.animateClick(msec=10)


class Worker(QtCore.QThread):

    playSignal = QtCore.Signal(mido.messages.messages.Message)

    def __init__(self):
        QtCore.QThread.__init__(self)

    def run(self):


        try:

            port_names =mido.get_output_names()

            nano = 'nanoPAD2'
            noct = 'Nocturn'

            for text in port_names:
                if nano in text:
                    inport = mido.open_input(text)
                    print('connected midi device:: ', text)
                elif noct in text:
                    inport = mido.open_input(text)
                    print('connected midi device:: ', text)


            for msg in inport:
                self.playSignal.emit(msg)
        except:
            print('NO MIDI DEVICE CONECTED. IF YOU WOULD LIKE TO CONNECT A MIDI DEVICE PLEASE CONNECT YOUR DEVICE AND RESTART THE APPLICATION. SUPPORTED DEVICES nanoPAD2, Nocturn ')





            #'nanoPAD2:nanoPAD2 MIDI 1 20:0'
           # inport = mido.open_input("Nocturn:Nocturn MIDI 1 20:0")



def main():
    app = QtGui.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main = sampler()
    main.show()
    app.exec_()

if __name__ == '__main__':
    main()
