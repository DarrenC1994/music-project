# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'practice_gui.ui'
#
# Created: Tue Dec  5 15:47:49 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(638, 504)
        MainWindow.setStyleSheet("background-color: rgb(0, 0, 0);\n"
"\n"
"\n"
"\n"
"")
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pad_2_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_2_btn.setGeometry(QtCore.QRect(300, 340, 93, 61))
        self.pad_2_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_2_btn.setObjectName("pad_2_btn")
        self.pad_10_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_10_btn.setGeometry(QtCore.QRect(300, 200, 93, 61))
        self.pad_10_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_10_btn.setObjectName("pad_10_btn")
        self.pad_16_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_16_btn.setGeometry(QtCore.QRect(520, 130, 93, 61))
        self.pad_16_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_16_btn.setObjectName("pad_16_btn")
        self.pad_3_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_3_btn.setGeometry(QtCore.QRect(410, 340, 93, 61))
        self.pad_3_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_3_btn.setObjectName("pad_3_btn")
        self.pushButton_17 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_17.setGeometry(QtCore.QRect(20, 130, 93, 61))
        self.pushButton_17.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.pushButton_17.setObjectName("pushButton_17")
        self.pad_13_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_13_btn.setGeometry(QtCore.QRect(190, 130, 93, 61))
        self.pad_13_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_13_btn.setObjectName("pad_13_btn")
        self.reverb_header = QtGui.QTextEdit(self.centralwidget)
        self.reverb_header.setGeometry(QtCore.QRect(200, 20, 71, 31))
        self.reverb_header.setStyleSheet("color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;")
        self.reverb_header.setObjectName("reverb_header")
        self.chorus_header = QtGui.QTextEdit(self.centralwidget)
        self.chorus_header.setGeometry(QtCore.QRect(310, 20, 71, 31))
        self.chorus_header.setStyleSheet("color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;")
        self.chorus_header.setObjectName("chorus_header")
        self.pad_7_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_7_btn.setGeometry(QtCore.QRect(410, 270, 93, 61))
        self.pad_7_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_7_btn.setObjectName("pad_7_btn")
        self.pad_1_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_1_btn.setGeometry(QtCore.QRect(190, 340, 93, 61))
        self.pad_1_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_1_btn.setObjectName("pad_1_btn")
        self.pad_14_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_14_btn.setGeometry(QtCore.QRect(300, 130, 93, 61))
        self.pad_14_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_14_btn.setObjectName("pad_14_btn")
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(140, 130, 20, 271))
        self.line.setStyleSheet("background-color:rgb(85, 0, 0);\n"
"")
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.pushButton_19 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_19.setGeometry(QtCore.QRect(20, 340, 93, 61))
        self.pushButton_19.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.pushButton_19.setObjectName("pushButton_19")
        self.Reverb = QtGui.QDial(self.centralwidget)
        self.Reverb.setGeometry(QtCore.QRect(210, 50, 50, 64))
        self.Reverb.setStyleSheet("background-color: rgb(0, 0, 127);")
        self.Reverb.setObjectName("Reverb")
        self.pad_9_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_9_btn.setGeometry(QtCore.QRect(190, 200, 93, 61))
        self.pad_9_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_9_btn.setObjectName("pad_9_btn")
        self.vol_header = QtGui.QTextEdit(self.centralwidget)
        self.vol_header.setGeometry(QtCore.QRect(520, 20, 71, 31))
        self.vol_header.setStyleSheet("color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;")
        self.vol_header.setObjectName("vol_header")
        self.pad_15_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_15_btn.setGeometry(QtCore.QRect(410, 130, 93, 61))
        self.pad_15_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_15_btn.setObjectName("pad_15_btn")
        self.pad_4_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_4_btn.setGeometry(QtCore.QRect(520, 340, 93, 61))
        self.pad_4_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_4_btn.setObjectName("pad_4_btn")
        self.Chorus = QtGui.QDial(self.centralwidget)
        self.Chorus.setGeometry(QtCore.QRect(320, 50, 50, 64))
        self.Chorus.setStyleSheet("background-color: rgb(0, 0, 127);")
        self.Chorus.setObjectName("Chorus")
        self.pad_11_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_11_btn.setGeometry(QtCore.QRect(410, 200, 93, 61))
        self.pad_11_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_11_btn.setObjectName("pad_11_btn")
        self.Volume = QtGui.QDial(self.centralwidget)
        self.Volume.setGeometry(QtCore.QRect(530, 50, 50, 64))
        self.Volume.setStyleSheet("background-color: rgb(0, 0, 127);")
        self.Volume.setObjectName("Volume")
        self.delay_header = QtGui.QTextEdit(self.centralwidget)
        self.delay_header.setGeometry(QtCore.QRect(420, 20, 71, 31))
        self.delay_header.setStyleSheet("color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;")
        self.delay_header.setObjectName("delay_header")
        self.Delay = QtGui.QDial(self.centralwidget)
        self.Delay.setGeometry(QtCore.QRect(430, 50, 50, 64))
        self.Delay.setStyleSheet("background-color: rgb(0, 0, 127);")
        self.Delay.setObjectName("Delay")
        self.pushButton_18 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_18.setGeometry(QtCore.QRect(20, 200, 93, 61))
        self.pushButton_18.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.pushButton_18.setObjectName("pushButton_18")
        self.pushButton_13 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_13.setGeometry(QtCore.QRect(20, 270, 93, 61))
        self.pushButton_13.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"border-width: 1px;\n"
"border-color: #339;\n"
"border-style: solid;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.pushButton_13.setObjectName("pushButton_13")
        self.pad_6_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_6_btn.setGeometry(QtCore.QRect(300, 270, 93, 61))
        self.pad_6_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_6_btn.setObjectName("pad_6_btn")
        self.pad_12_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_12_btn.setGeometry(QtCore.QRect(520, 200, 93, 61))
        self.pad_12_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_12_btn.setObjectName("pad_12_btn")
        self.pad_5_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_5_btn.setGeometry(QtCore.QRect(190, 270, 93, 61))
        self.pad_5_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_5_btn.setObjectName("pad_5_btn")
        self.pad_8_btn = QtGui.QPushButton(self.centralwidget)
        self.pad_8_btn.setGeometry(QtCore.QRect(520, 270, 93, 61))
        self.pad_8_btn.setStyleSheet("QPushButton {\n"
"color: white;\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff710a, stop: 0.1 #ff710a, stop: 0.49#ff710a, stop: 0.5#ff710a, stop: 1#ffb366);\n"
"border-width: 1px;\n"
"border-color: #ff710a;\n"
"border-style: solid;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"    background-color: rgb(243, 255, 0);\n"
"\n"
"}")
        self.pad_8_btn.setObjectName("pad_8_btn")
        self.verticalLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 10, 161, 111))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.play_button = QtGui.QRadioButton(self.verticalLayoutWidget)
        self.play_button.setEnabled(True)
        self.play_button.setStyleSheet("\n"
"color: rgb(170, 0, 0);")
        self.play_button.setChecked(True)
        self.play_button.setObjectName("play_button")
        self.verticalLayout.addWidget(self.play_button)
        self.edit_button = QtGui.QRadioButton(self.verticalLayoutWidget)
        self.edit_button.setStyleSheet("color: rgb(170, 0, 0);")
        self.edit_button.setChecked(False)
        self.edit_button.setObjectName("edit_button")
        self.verticalLayout.addWidget(self.edit_button)
        self.load_button = QtGui.QRadioButton(self.verticalLayoutWidget)
        self.load_button.setStyleSheet("color: rgb(170, 0, 0);")
        self.load_button.setObjectName("load_button")
        self.verticalLayout.addWidget(self.load_button)
        self.horizontalLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(20, 420, 591, 41))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.record_button = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.record_button.setStyleSheet("QPushButton\n"
"{\n"
"background-color: rgb(170, 255, 255);\n"
"}\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.record_button.setObjectName("record_button")
        self.horizontalLayout.addWidget(self.record_button)
        self.play_loop_button = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.play_loop_button.setStyleSheet("\n"
"QPushButton\n"
"{\n"
"background-color: rgb(85, 255, 255);\n"
"\n"
"}\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.play_loop_button.setObjectName("play_loop_button")
        self.horizontalLayout.addWidget(self.play_loop_button)
        self.delete_loop_button = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.delete_loop_button.setStyleSheet("QPushButton\n"
"{\n"
"background-color: rgb(170, 255, 255);\n"
"}\n"
"QPushButton:pressed{\n"
"\n"
"    background-color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.delete_loop_button.setObjectName("delete_loop_button")
        self.horizontalLayout.addWidget(self.delete_loop_button)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 638, 25))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_2_btn.setText(QtGui.QApplication.translate("MainWindow", "X", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_10_btn.setText(QtGui.QApplication.translate("MainWindow", "W", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_16_btn.setText(QtGui.QApplication.translate("MainWindow", "4", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_3_btn.setText(QtGui.QApplication.translate("MainWindow", "C", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_17.setText(QtGui.QApplication.translate("MainWindow", "DANCE", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_13_btn.setText(QtGui.QApplication.translate("MainWindow", "1", None, QtGui.QApplication.UnicodeUTF8))
        self.reverb_header.setHtml(QtGui.QApplication.translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:8pt;\">Reverb</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.chorus_header.setHtml(QtGui.QApplication.translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:8pt;\">Chorus</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_7_btn.setText(QtGui.QApplication.translate("MainWindow", "D", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_1_btn.setText(QtGui.QApplication.translate("MainWindow", "Z", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_14_btn.setText(QtGui.QApplication.translate("MainWindow", "2", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_19.setText(QtGui.QApplication.translate("MainWindow", "REGEA", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_9_btn.setText(QtGui.QApplication.translate("MainWindow", "Q", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_header.setHtml(QtGui.QApplication.translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:8pt;\">Volume</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_15_btn.setText(QtGui.QApplication.translate("MainWindow", "3", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_4_btn.setText(QtGui.QApplication.translate("MainWindow", "V", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_11_btn.setText(QtGui.QApplication.translate("MainWindow", "E", None, QtGui.QApplication.UnicodeUTF8))
        self.delay_header.setHtml(QtGui.QApplication.translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:8pt;\">Delay</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_18.setText(QtGui.QApplication.translate("MainWindow", "HIP HOP", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_13.setText(QtGui.QApplication.translate("MainWindow", "HOUSE", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_6_btn.setText(QtGui.QApplication.translate("MainWindow", "S", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_12_btn.setText(QtGui.QApplication.translate("MainWindow", "R", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_5_btn.setText(QtGui.QApplication.translate("MainWindow", "A", None, QtGui.QApplication.UnicodeUTF8))
        self.pad_8_btn.setText(QtGui.QApplication.translate("MainWindow", "F", None, QtGui.QApplication.UnicodeUTF8))
        self.play_button.setText(QtGui.QApplication.translate("MainWindow", "Play", None, QtGui.QApplication.UnicodeUTF8))
        self.edit_button.setText(QtGui.QApplication.translate("MainWindow", "Edit", None, QtGui.QApplication.UnicodeUTF8))
        self.load_button.setText(QtGui.QApplication.translate("MainWindow", "Load", None, QtGui.QApplication.UnicodeUTF8))
        self.record_button.setText(QtGui.QApplication.translate("MainWindow", "Record", None, QtGui.QApplication.UnicodeUTF8))
        self.play_loop_button.setText(QtGui.QApplication.translate("MainWindow", "Play", None, QtGui.QApplication.UnicodeUTF8))
        self.delete_loop_button.setText(QtGui.QApplication.translate("MainWindow", "Delete", None, QtGui.QApplication.UnicodeUTF8))

