import threading
import sys
import sox


class pad:
    def __init__(self, wav_fname, effect_fname):
        if ".wav" not in wav_fname:
            print("not a wav file")
            sys.exit()

        self.wav_fname = wav_fname
        self.effect_fname = effect_fname
        self.effect_flag = False

    def play(self):
        if not self.effect_flag:
            self.sound = self.wav_fname
        else:
            self.sound = self.effect_fname
        try:
            threading.Thread(target=self.play_sound, ).start()
        except:
            print("could not play", self.sound)

    def play_sound(self):
        s = [self.sound]
        sox.core.play(s)

    def reverb(self, start, end):
        pass

    def echo(self, amount):
        pass

    def delay(self, amount):
        pass

    def trim_length(self, amount):
        pass
