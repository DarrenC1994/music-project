from PySide import QtCore, QtGui
import sampler_gui
import sys
from pad import pad
from effects import Effects


class sampler(QtGui.QMainWindow, sampler_gui.Ui_MainWindow):

    def __init__(self, parent=None):
        super(sampler, self).__init__(parent)
        self.setupUi(self)

        # flag to see if effects are applied
        # this is select the track to play

        self.pad_1_has_effect = 0
        self.pad_2_has_effect = 0
        self.pad_3_has_effect = 0
        self.pad_4_has_effect = 0
        self.pad_5_has_effect = 0
        self.pad_6_has_effect = 0
        self.pad_7_has_effect = 0
        self.pad_8_has_effect = 0
        self.pad_9_has_effect = 0
        self.pad_10_has_effect = 0
        self.pad_11_has_effect = 0
        self.pad_12_has_effect = 0
        self.pad_13_has_effect = 0
        self.pad_14_has_effect = 0
        self.pad_15_has_effect = 0
        self.pad_16_has_effect = 0


        self.edit_button.toggled.connect(lambda: self.btnstate(self.edit_button))
        self.play_button.toggled.connect(lambda: self.btnstate(self.edit_button))
        self.load_button.toggled.connect(lambda: self.btnstate(self.edit_button))






        #the default track to be played on clicking a button
        #if the has_effect flag is 0 for the button
        self_pad_1_track=  "kick.wav"
        self_pad_2_track=  "snare.wav"
        self_pad_3_track=  "hh.wav"
        self_pad_4_track=  "samples/Drums/Metallic/Agogo Digital DDM220 1.wav"
        self_pad_5_track=  "samples/Drums/Metallic/Cow TapeEcho V1.wav"
        self_pad_6_track=  "samples/Drums/Metallic/Triangle DC2 3.wav"
        self_pad_7_track=  "samples/Drums/Snare/Rim Analog DR55.wav"
        self_pad_8_track=  "samples/Drums/Metallic/Cow GBTattoo.wav"
        self_pad_9_track=  "samples/Drums/Snare/Snare Live 6 bit Meat.wav"
        self_pad_10_track=  "samples/Drums/Snare/Snare Mpc60 clipped 016.wav"
        self_pad_11_track=  "samples/Drums/Tom/Tom Analog JX3P 3.wav"
        self_pad_12_track=  "samples/Drums/Tom/Tom RealBlueDrumA3.wav"
        self_pad_13_track=  "samples/Drums/Wooden/Clave Analog MS20.wav"
        self_pad_14_track=  "samples/Drums/Wooden/Perc Tass Dann 1.wav"
        self_pad_15_track=  "samples/Drums/Shaker/Shaker Digital RX5 1.wav"
        self_pad_16_track=  "samples/Drums/Shaker/Shaker Buki Kat.wav"

        self.location_of_defaults =[
        self_pad_1_track,
        self_pad_2_track,
        self_pad_3_track,
        self_pad_4_track,
        self_pad_5_track,
        self_pad_6_track,
        self_pad_7_track,
        self_pad_8_track,
        self_pad_9_track,
        self_pad_10_track,
        self_pad_11_track,
        self_pad_12_track,
        self_pad_13_track,
        self_pad_14_track,
        self_pad_15_track,
        self_pad_16_track]

        #the track to be played if the has_effect flag for the button is not 0
        #
        b1effects = 'samples//Test//Processed//b1.wav'
        b2effects = 'samples//Test//Processed//b2.wav'
        b3effects = 'samples//Test//Processed//b3.wav'
        b4effects = 'samples//Test//Processed//b4.wav'
        b5effects = 'samples//Test//Processed//b5.wav'
        b6effects = 'samples//Test//Processed//b6.wav'
        b7effects = 'samples//Test//Processed//b7.wav'
        b8effects = 'samples//Test//Processed//b8.wav'
        b9effects = 'samples//Test//Processed//b9.wav'
        b10effects = 'samples//Test//Processed//b10.wav'
        b11effects = 'samples//Test//Processed//b11.wav'
        b12effects = 'samples//Test//Processed//b12.wav'
        b13effects = 'samples//Test//Processed//b13.wav'
        b14effects = 'samples//Test//Processed//b14.wav'
        b15effects = 'samples//Test//Processed//b15.wav'
        b16effects = 'samples//Test//Processed//b16.wav'

        self.loction_of_effects_wav_for_each_button = [
            b1effects,
            b2effects,
            b3effects,
            b4effects,
            b5effects,
            b6effects,
            b7effects,
            b8effects,
            b9effects,
            b10effects,
            b11effects,
            b12effects,
            b13effects,
            b14effects,
            b15effects,
            b16effects
        ]



        # check if effects are applied and play correct track
        if self.pad_1_has_effect == 0:
            self.pad_1 = pad(self_pad_1_track)
            print("p1 no effects")
        else:
            self.pad_1 = pad(b1effects)
            print("p1 with effects")

        if self.pad_2_has_effect == 0:
            self.pad_2 = pad(self_pad_2_track)
            print("p2 no effects")
        else:
            self.pad_2 = pad(b2effects)
            print("p2 with effects")

        if self.pad_3_has_effect == 0:
            self.pad_3 = pad(self_pad_3_track)
            print("p3 no effects")
        else:
            self.pad_3 = pad(b3effects)
            print("p3 with effects")

        if self.pad_4_has_effect == 0:
            self.pad_4 = pad(self_pad_4_track)
            print("p4 no effects")
        else:
            self.pad_4 = pad(b4effects)
            print("p4 wth effects")

        if self.pad_5_has_effect == 0:
            self.pad_5 = pad(self_pad_5_track)
            print("p5 no effects")
        else:
            self.pad_5 = pad(b5effects)
            print("p5 wth effects")


        if self.pad_6_has_effect == 0:
            self.pad_6 = pad(self_pad_6_track)
            print("p6 no effects")
        else:
            self.pad_6 = pad(b6effects)
            print("p6 wth effects")

        if self.pad_7_has_effect == 0:
            self.pad_7 = pad(self_pad_7_track)
            print("p7 no effects")
        else:
            self.pad_7 = pad(b7effects)
            print("p7 wth effects")

        if self.pad_8_has_effect == 0:
            self.pad_8 = pad(self_pad_8_track)
            print("p8 no effects")
        else:
            self.pad_8 = pad(b8effects)
            print("p8 wth effects")

        if self.pad_9_has_effect == 0:
            self.pad_9 = pad(self_pad_9_track)
            print("p9 no effects")
        else:
            self.pad_9 = pad(b9effects)
            print("p9 with effects")


        if self.pad_10_has_effect == 0:
            self.pad_10 = pad(self_pad_10_track)
            print("p10 no effects")
        else:
            self.pad_10 = pad(b10effects)
            print("p10 with effects")

        if self.pad_11_has_effect == 0:
            self.pad_11 = pad(self_pad_11_track)
            print("p11 no effects")
        else:
            self.pad_11 = pad(b11effects)
            print("p11 with effects")

        if self.pad_12_has_effect == 0:
            self.pad_12 = pad(self_pad_12_track)
            print("p12 no effects")
        else:
            self.pad_12 = pad(b12effects)
            print("p12 with effects")

        if self.pad_13_has_effect == 0:
            self.pad_13 = pad(self_pad_13_track)
            print("p13 no effects")
        else:
            self.pad_13 = pad(b13effects)
            print("p13 with effects")

        if self.pad_14_has_effect == 0:
            self.pad_14 = pad(self_pad_14_track)
            print("p14 no effects")
        else:
            self.pad_14 = pad(b14effects)
            print("p14 with effects")

        if self.pad_15_has_effect == 0:
            self.pad_15 = pad(self_pad_15_track)
            print("p15 no effects")
        else:
            self.pad_15 = pad(b15effects)
            print("p15 with effects")

        if self.pad_16_has_effect == 0:
            self.pad_16 = pad(self_pad_16_track)
            print("p16 no effects")
        else:
            self.pad_16 = pad(b16effects)
            print("p16 with effects")


        self.pad_1_btn.clicked.connect(lambda: self.pad_(self.pad_1))
        self.pad_2_btn.clicked.connect(lambda: self.play_pad(self.pad_2))
        self.pad_3_btn.clicked.connect(lambda: self.play_pad(self.pad_3))
        self.pad_4_btn.clicked.connect(lambda: self.play_pad(self.pad_4))
        self.pad_5_btn.clicked.connect(lambda: self.play_pad(self.pad_5))
        self.pad_6_btn.clicked.connect(lambda: self.play_pad(self.pad_6))
        self.pad_7_btn.clicked.connect(lambda: self.play_pad(self.pad_7))
        self.pad_8_btn.clicked.connect(lambda: self.play_pad(self.pad_8))
        self.pad_9_btn.clicked.connect(lambda: self.play_pad(self.pad_9))
        self.pad_10_btn.clicked.connect(lambda: self.play_pad(self.pad_10))
        self.pad_11_btn.clicked.connect(lambda: self.play_pad(self.pad_11))
        self.pad_12_btn.clicked.connect(lambda: self.play_pad(self.pad_12))
        self.pad_13_btn.clicked.connect(lambda: self.play_pad(self.pad_13))
        self.pad_14_btn.clicked.connect(lambda: self.play_pad(self.pad_14))
        self.pad_15_btn.clicked.connect(lambda: self.play_pad(self.pad_15))
        self.pad_16_btn.clicked.connect(lambda: self.play_pad(self.pad_16))



        #
        # self.pad_2 = pad(self_pad_2_track)
        # self.pad_3 = pad(self_pad_3_track)
        #
        # if self.pad_4_has_effect == 0:
        #    self.pad_4 = pad(
        #                  self_pad_4_track)
        # else:
        #     self.pad_4 = pad(
        #         "samples/Test/Processed/with_reverb.wav")
        #
        #
        # self.pad_5 = pad(self_pad_5_track)
        # self.pad_6 = pad(self_pad_6_track)
        # self.pad_7 = pad(self_pad_7_track)
        # self.pad_8 = pad(self_pad_8_track)
        # self.pad_9 = pad(self_pad_9_track)
        # self.pad_10 = pad(self_pad_10_track)
        # self.pad_11 = pad(self_pad_11_track)
        # self.pad_12 = pad(self_pad_12_track)
        # self.pad_13 = pad(self_pad_13_track)
        # self.pad_14 = pad(self_pad_14_track)
        # self.pad_15 = pad(self_pad_15_track)
        # self.pad_16 = pad(self_pad_16_track)



        #
        # if self.play_button.isChecked():
        #
        #     print("in play mode")
        #     self.pad_1_btn.clicked.connect(lambda: self.play_pad(self.pad_1))
        #     self.pad_2_btn.clicked.connect(lambda: self.play_pad(self.pad_2))
        #     self.pad_3_btn.clicked.connect(lambda: self.play_pad(self.pad_3))
        #     self.pad_4_btn.clicked.connect(lambda: self.play_pad(self.pad_4))
        #     self.pad_5_btn.clicked.connect(lambda: self.play_pad(self.pad_5))
        #     self.pad_6_btn.clicked.connect(lambda: self.play_pad(self.pad_6))
        #     self.pad_7_btn.clicked.connect(lambda: self.play_pad(self.pad_7))
        #     self.pad_8_btn.clicked.connect(lambda: self.play_pad(self.pad_8))
        #     self.pad_9_btn.clicked.connect(lambda: self.play_pad(self.pad_9))
        #     self.pad_10_btn.clicked.connect(lambda: self.play_pad(self.pad_10))
        #     self.pad_11_btn.clicked.connect(lambda: self.play_pad(self.pad_11))
        #     self.pad_12_btn.clicked.connect(lambda: self.play_pad(self.pad_12))
        #     self.pad_13_btn.clicked.connect(lambda: self.play_pad(self.pad_13))
        #     self.pad_14_btn.clicked.connect(lambda: self.play_pad(self.pad_14))
        #     self.pad_15_btn.clicked.connect(lambda: self.play_pad(self.pad_15))
        #     self.pad_16_btn.clicked.connect(lambda: self.play_pad(self.pad_16))
        #
        # elif self.edit_button.isChecked():
        #     print("in edit mode")
        #
        # elif self.load_button:
        #     print("in load mode")
        #




    def btnstate(self, pad):

        edit =self.edit_button.isChecked()
        play = self.play_button.isChecked()
        load = self.load_button.isChecked()

        print(self.edit_button.isChecked(),
        self.play_button.isChecked(),
        self.load_button.isChecked())

        if play == True:



            print("in play mode")
            self.pad_1_btn.clicked.connect(lambda: self.play_pad(self.pad_1))
            self.pad_2_btn.clicked.connect(lambda: self.play_pad(self.pad_2))
            self.pad_3_btn.clicked.connect(lambda: self.play_pad(self.pad_3))
            self.pad_4_btn.clicked.connect(lambda: self.play_pad(self.pad_4))
            self.pad_5_btn.clicked.connect(lambda: self.play_pad(self.pad_5))
            self.pad_6_btn.clicked.connect(lambda: self.play_pad(self.pad_6))
            self.pad_7_btn.clicked.connect(lambda: self.play_pad(self.pad_7))
            self.pad_8_btn.clicked.connect(lambda: self.play_pad(self.pad_8))
            self.pad_9_btn.clicked.connect(lambda: self.play_pad(self.pad_9))
            self.pad_10_btn.clicked.connect(lambda: self.play_pad(self.pad_10))
            self.pad_11_btn.clicked.connect(lambda: self.play_pad(self.pad_11))
            self.pad_12_btn.clicked.connect(lambda: self.play_pad(self.pad_12))
            self.pad_13_btn.clicked.connect(lambda: self.play_pad(self.pad_13))
            self.pad_14_btn.clicked.connect(lambda: self.play_pad(self.pad_14))
            self.pad_15_btn.clicked.connect(lambda: self.play_pad(self.pad_15))
            self.pad_16_btn.clicked.connect(lambda: self.play_pad(self.pad_16))

        elif edit == True:
            print("in edit mode")
            self.pad_1_btn.clicked.connect(lambda: self.add_effects(self.location_of_defaults[0],
                                                                    self.loction_of_effects_wav_for_each_button[0], self.pad_1_has_effect))
            self.pad_2_btn.clicked.connect(lambda: self.add_effects(self.location_of_defaults[1],
                                                                    self.loction_of_effects_wav_for_each_button[1],self.pad_2_has_effect))
            self.pad_3_btn.clicked.connect(lambda: self.add_effects(self.location_of_defaults[2],
                                                                    self.loction_of_effects_wav_for_each_button[2],self.pad_3_has_effect))
            # self.pad_2_btn.clicked.connect(lambda: self.play_pad(self.pad_2))
            # self.pad_3_btn.clicked.connect(lambda: self.play_pad(self.pad_3))
            # self.pad_4_btn.clicked.connect(lambda: self.play_pad(self.pad_4))
            # self.pad_5_btn.clicked.connect(lambda: self.play_pad(self.pad_5))
            # self.pad_6_btn.clicked.connect(lambda: self.play_pad(self.pad_6))
            # self.pad_7_btn.clicked.connect(lambda: self.play_pad(self.pad_7))
            # self.pad_8_btn.clicked.connect(lambda: self.play_pad(self.pad_8))
            # self.pad_9_btn.clicked.connect(lambda: self.play_pad(self.pad_9))
            # self.pad_10_btn.clicked.connect(lambda: self.play_pad(self.pad_10))
            # self.pad_11_btn.clicked.connect(lambda: self.play_pad(self.pad_11))
            # self.pad_12_btn.clicked.connect(lambda: self.play_pad(self.pad_12))
            # self.pad_13_btn.clicked.connect(lambda: self.play_pad(self.pad_13))
            # self.pad_14_btn.clicked.connect(lambda: self.play_pad(self.pad_14))
            # self.pad_15_btn.clicked.connect(lambda: self.play_pad(self.pad_15))
            # self.pad_16_btn.clicked.connect(lambda: self.play_pad(self.pad_16))
        elif load == True:

            print("in load mode")

    ######################################################################################


    def play_pad(self, pad):

        pad.play()
     #######################################################################

    def add_effects(self, default_loc, effects_loc, effects_flag):

        self.effects_flag= 1
        ef = Effects(default_loc, effects_loc)
        effects_dict = {"reverb":10, "chorus":.1}
        ef.effects_to_be_added(effects_dict)
        ef.apply_effects_to_file()






    #####################################################################
    def keyPressEvent(self, e):



        if e.text() in ("a"):

            self.pad_1_btn.animateClick()

        elif e.text() in ("b"):
            self.pad_2_btn.animateClick()

        elif e.text() in ("c"):
            self.pad_3_btn.animateClick()

        elif e.text() in ("d"):
            self.pad_4_btn.animateClick()

        elif e.text() in ("e"):
            self.pad_5_btn.animateClick()

        elif e.text() in ("f"):
            self.pad_6_btn.animateClick()

        elif e.text() in ("g"):
            self.pad_7_btn.animateClick()

        elif e.text() in ("h"):
            self.pad_8_btn.animateClick()

        elif e.text() in ("i"):
            self.pad_9_btn.animateClick()

        elif e.text() in ("j"):
            self.pad_10_btn.animateClick()

        elif e.text() in ("k"):
            self.pad_11_btn.animateClick()

        elif e.text() in ("l"):
            self.pad_12_btn.animateClick()

        elif e.text() in ("m"):
            self.pad_13_btn.animateClick()

        elif e.text() in ("n"):
            self.pad_14_btn.animateClick()

        elif e.text() in ("o"):
            self.pad_15_btn.animateClick()

        elif e.text() in ("p"):
            self.pad_16_btn.animateClick()

        elif e.text() in ("edit"):
            self.edit_button.animateClick()



def main():
    app = QtGui.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main = sampler()
    main.show()
    app.exec_()

if __name__ == '__main__':
    main()
